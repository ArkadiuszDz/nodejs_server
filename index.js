const http = require('http');
const url = require('url');
const actimel_handlers = require('./actimel/actimel_handlers');
const actimel_router = require('./actimel/actimel_router');
const api_router = require('./actimel/api_router');
const novanoc_handlers = require('./novanoc/novanoc_handlers');
const ActimelHandler = require('./actimel/actimel');
const path = require('path');

const actimel_handler = new ActimelHandler('actimel', 'actimel/public');

const server = http.createServer(function (req,res) {
    
    const parsedUrl = url.parse(req.url, true);
    const path_url = parsedUrl.pathname;
    const trimmedPath = path_url.replace(/^\/+|\/+$/g,'');
    const method = req.method.toLowerCase();
    const headers = req.headers;
    const ext = path.parse(trimmedPath).ext;

    if (method === 'get') {
        console.log('Origin', headers.origin);
        if (headers.host.indexOf('actimel') != -1) {
            if (typeof (actimel_router[trimmedPath]) !== 'undefined') {
                //actimel_handlers.view_handler(trimmedPath, res)
                actimel_handler.view_handler(trimmedPath, res);
             } else if (!!ext) {
                actimel_handler.asset_handler(trimmedPath, res);
             } else if (trimmedPath.indexOf('zwyciezca') != -1) {
                actimel_handlers.winner_handler(res);
             } else {
                actimel_handlers.not_found(trimmedPath);
             }
        }

        if (headers.host.indexOf('novanoc') != -1) {
            
          if (!!ext) {
            novanoc_handlers.asset_handler(trimmedPath, req, res);
          } else {
            novanoc_handlers.view_handler(trimmedPath, req, res);
          }
    
        }

    }

    if (method === 'post') {
      const queryStringObject = parsedUrl.query;
      let chosen_handler = api_router[trimmedPath];
      chosen_handler(req, res);
    }

});


server.listen(3000, function(){
    console.log('Server is listening on port 3000');
});