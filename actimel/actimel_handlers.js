const fs = require('fs');
const path = require('path');
const renderFile = require('node-twig').renderFile;
const lottery_info = require('./lottery');
const actimel_router = require('./actimel_router');
const mimeType = require('../config/mime');

const options = {
                  root: `${__dirname}/public`,
                  context: {}
                };


const actimel_handlers = {
  view_handler(trimmedPath, request, response) {

    let endpoint = actimel_router[trimmedPath].template_path;
    console.log(lottery_info.lottery());
    if (actimel_router[trimmedPath].restricted !== 'none' && actimel_router[trimmedPath].restricted !== lottery_info.lottery().state) {
      response.writeHead(302, { 'Location': actimel_router[trimmedPath].redirect[lottery_info.lottery().state] });
      response.end();
    }

    if (lottery_info.lottery().state === 'paused') {
      options.context = {
        currentDate: lottery_info.lottery().currentDate,
        pausedTill: lottery_info.lottery().pausedTill
      }
    }
    
    if (fs.existsSync(path.join(__dirname,endpoint))) {
      renderFile(endpoint, options, (error, template) => {
        response.writeHead(200,{"Content-type" : 'text/html'});
        response.end(template);
      });
    }

  },
  winner_handler(response) {
    let endpoint = actimel_router['zwyciezca'].template_path;
    if (fs.existsSync(path.join(__dirname,endpoint))) {
      renderFile(endpoint, options, (error, template) => {
        response.writeHead(200,{"Content-type" : 'text/html'});
        console.log(template);
        response.end(template);
      });
    }
  },
  asset_handler(trimmedPath, request, response) {
    // let file = fs.existsSync(path.join(__dirname,`public/${trimmedPath}`)) ? fs.readFileSync(path.join(__dirname,`public/${trimmedPath}`)) : null;
    let file = fs.existsSync(path.join(__dirname,`public/${trimmedPath}`)) ? fs.createReadStream(path.join(__dirname,`public/${trimmedPath}`)) : null;
    let ext = path.parse(trimmedPath).ext;
    if (file) {
      response.statusCode = 200;
      response.setHeader('Content-type', mimeType[ext] || 'text/plain' );
      // file.pipe(response);
      file.on('data',(chunk) => {
        response.write(chunk);
        console.log(chunk);
      });
      file.on('end',() => {
        response.end();
        console.log('Koniec');
      });
      // response.end(file);
    }
    
  },
  not_found(trimmedPath, response) {
    console.log(404, trimmedPath);
    response.statusCode = 404;
    response.end();
  }
};

module.exports = actimel_handlers;