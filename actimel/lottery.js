const lottery = () => {
  const lottery_start = new Date('2019/06/03');
  lottery_start.setHours(10);
  lottery_start.setMinutes(0);

  const lottery_end = new Date('2019/06/30');
  lottery_end.setHours(19);
  lottery_end.setMinutes(0);

  const now = new Date();
  const lottery_open = new Date();

  lottery_open.setHours(10);
  lottery_open.setMinutes(1);
  lottery_open.setSeconds(0);

  const lottery_pause = new Date();

  lottery_pause.setHours(19);
  lottery_pause.setMinutes(0);

  if (now >= lottery_pause) {
    lottery_open.setDate(now.getDate() + 1);
  }

  if (now >= lottery_start && now <= lottery_end && now >= lottery_open && now <= lottery_pause) {
    return { state: 'open' };
  } else if (now < lottery_open && now <= lottery_end || now <= lottery_end && now > lottery_pause) {
    return { state: 'paused',
            currentDate: now.toISOString().split('T')[0].replace(/-/g,'/') + ' ' + now.toLocaleTimeString(),
            pausedTill: lottery_open.toISOString().split('T')[0].replace(/-/g,'/') + ' ' + lottery_open.toLocaleTimeString() }
  } else if (now >= lottery_end) {
    return { state: 'finished' }
  } else if (now < lottery_start) {
    return { state: 'unopened' }
  }

}

const lottery_info = { lottery }

module.exports = lottery_info;