const BasicHandler = require('../basic_handler');
const fs = require('fs');
const path = require('path');
const renderFile = require('node-twig').renderFile;
const lottery_info = require('./lottery');
const actimel_router = require('./actimel_router');

const options = {
  root: `${__dirname}/public`,
  context: {}
};



class ActimelHandler extends BasicHandler {
  view_handler(trimmedPath, response) {
    let endpoint = actimel_router[trimmedPath].template_path;
    console.log(lottery_info.lottery());

    if (actimel_router[trimmedPath].restricted !== 'none' && actimel_router[trimmedPath].restricted !== lottery_info.lottery().state) {
      response.writeHead(302, { 'Location': actimel_router[trimmedPath].redirect[lottery_info.lottery().state] });
      response.end();
    }

    if (lottery_info.lottery().state === 'paused') {
      options.context = {
        currentDate: lottery_info.lottery().currentDate,
        pausedTill: lottery_info.lottery().pausedTill
      }
    }
    
    if (fs.existsSync(path.join(__dirname,endpoint))) {
      renderFile(endpoint, options, (error, template) => {
        response.writeHead(200,{"Content-type" : 'text/html'});
        console.log(template);
        response.end(template);
      });
    }
  }
  not_found(trimmedPath, response) {
    console.log(404, trimmedPath);
    response.status(404).end();
  }
}

module.exports = ActimelHandler;