const actimel_router = {
  '' : {
        template_path: 'public/templates/page/main/index.html.twig',
        restricted: 'open',
        redirect: {
          paused: '/loteria-zamknieta',
          finished: '/loteria-zakonczona',
          unopened: '/loteria-nierozpoczeta'
        }
      },
  'wez-udzial' : {
                  template_path: 'public/templates/page/main/lottery-form.html.twig',
                  restricted: 'open',
                  redirect: {
                    paused: '/loteria-zamknieta',
                    finished: '/loteria-zakonczona',
                    unopened: '/loteria-nierozpoczeta'
                  }
                },
  'kontakt' : {
                template_path: 'public/templates/page/main/contact.html.twig',
                restricted: 'none'
            },
  'polityka-prywatnosci' : {
                            template_path: 'public/templates/page/main/privacy-policy.html.twig',
                            restricted: 'none'
                          },
  'sprobuj-ponownie' : {
                        template_path: 'public/templates/page/main/try-again.html.twig',
                        restricted: 'open',
                        redirect: {
                          paused: '/loteria-zamknieta',
                          finished: '/loteria-zakonczona',
                          unopened: '/loteria-nierozpoczeta'
                        }
                      },
  'loteria-zakonczona' : {
                          template_path: 'public/templates/page/main/lottery-finished.html.twig',
                          restricted: 'finished',
                          redirect: {
                            open: '/',
                            unopened: '/loteria-nierozpoczeta',
                            paused: '/loteria-zamknieta'
                          }
                        },
  'loteria-zamknieta' : {
                        template_path: 'public/templates/page/main/lottery-paused.html.twig',
                        restricted: 'paused',
                        redirect: {
                          open: '/',
                          unopened: '/loteria-nierozpoczeta',
                          finished: '/loteria-zakonczona'
                        }
                      },
  'loteria-nierozpoczeta': {
                            template_path: 'public/templates/page/main/lottery-unopened.html.twig',
                            restricted: 'unopened',
                            redirect: {
                              open: '/',
                              paused: '/loteria-zamknieta',
                              finished: '/loteria-zakonczona'
                            }
                          },
  'zwyciezca' : {
                  template_path: 'public/templates/page/main/winner-form.html.twig',
                  restricted: 'none', // dla widoku zwyciezcy zrobić handler
  }
};

module.exports = actimel_router;