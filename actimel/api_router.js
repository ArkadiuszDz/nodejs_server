const api_handlers = require('./api_handlers');


const api_router =  {

  'application/create': api_handlers.create_handler,
  'application/winner/data': api_handlers.winner_handler,
  'application/winner/image': api_handlers.image_handler

}

module.exports = api_router;