const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf-8');
const crypto = require('crypto');

const api_handlers = {
  create_handler(request,response) {
    let payload = '';

    request.on('data',function(chunk){
        payload += decoder.write(chunk);
    });

    request.on('end',function(){
        payload += decoder.end();
        payload = JSON.parse(payload);
        let base_string = `${payload["cashRegisterNumber"]} ${payload["printoutNumber"]} ${payload["printoutDate"]}`;
        let hash = crypto.createHmac('sha256', base_string).digest('hex');

        response.writeHead(201,{
          "content-type" : 'application/json',
          "status": 201
        });
        response.write(JSON.stringify({winner: true, winnerFormUrl: `/zwyciezca/${hash}`}));
        response.end();

    });

  },
  winner_handler(request) {
    console.log(request);
  },
  image_handler(request) {
    console.log(request);
  }
}

module.exports = api_handlers;