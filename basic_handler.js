const fs = require('fs');
const path = require('path');

class BasicHandler {
  constructor(domain, root) {
    this.domain = domain;
    this.root = root;
  }
  mimeType() {
    return {
      '.ico': 'image/x-icon',
      '.html': 'text/html',
      '.js': 'text/javascript',
      '.json': 'application/json',
      '.css': 'text/css',
      '.png': 'image/png',
      '.jpg': 'image/jpeg',
      '.wav': 'audio/wav',
      '.mp3': 'audio/mpeg',
      '.svg': 'image/svg+xml',
      '.pdf': 'application/pdf',
      '.doc': 'application/msword',
      '.eot': 'appliaction/vnd.ms-fontobject',
      '.ttf': 'aplication/font-sfnt'
    }
  }
  asset_handler(trimmedPath, response) {
    let file = fs.existsSync(path.join(__dirname,`${this.root}/${trimmedPath}`)) ? fs.createReadStream(path.join(__dirname,`${this.root}/${trimmedPath}`)) : null;
    let ext = path.parse(trimmedPath).ext;

    if (file) {
      response.statusCode = 200;
      response.setHeader('Content-type', this.mimeType()[ext] || 'text/plain' );

      file.on('data',(chunk) => {
        response.write(chunk);
      });
      file.on('end',() => {
        response.end();
      });

    }
    
  }
  favicon_handler(trimmedPath, response) {
    let file = fs.existsSync(path.join(__dirname,`${this.root}/${trimmedPath}`)) ? fs.readFileSync(path.join(__dirname,`${this.root}/${trimmedPath}`)) : null;
    let ext = path.parse(trimmedPath).ext;

    if (file) {
      response.statusCode = 200;
      response.setHeader('Content-type', this.mimeType()[ext] || 'text/plain' );
      response.end(file);
    }
  }
}

module.exports = BasicHandler;