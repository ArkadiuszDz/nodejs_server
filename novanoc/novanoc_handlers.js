const novanoc_router = require('./novanoc_router');
const fs = require('fs');
const path = require('path');
const mimeType = require('../config/mime');


const novanoc_handlers = {
  view_handler(trimmedPath,request, response) {

    let ext = path.parse(trimmedPath).ext;
    let endpoint;

    if (!!ext) {
      endpoint = `public/${trimmedPath}`;
    } else if (!!trimmedPath) {
      endpoint = `public/${trimmedPath}/index.html`;
    } else {
      endpoint = 'public/index.html';
    }

    if (fs.existsSync(path.join(__dirname,endpoint))) {
      
      let file = fs.readFileSync(path.join(__dirname, endpoint));
      response.statusCode = 200;
      response.setHeader('Content-type', mimeType[ext] || 'text/html' );
      response.end(file);

    }

  },
  asset_handler(trimmedPath, request, response) {
    
    let file = fs.existsSync(path.join(__dirname,`public/${trimmedPath}`)) ? fs.createReadStream(path.join(__dirname,`public/${trimmedPath}`)) : null;
    let ext = path.parse(trimmedPath).ext;
    if (file) {
      response.statusCode = 200;
      response.setHeader('Content-type', mimeType[ext] || 'text/plain' );
      file.on('data',(chunk) => {
        response.write(chunk);
        console.log(chunk);
      });
      file.on('end',() => {
        response.end();

      });
 
    }
    
  },
  favicon_handler() {
    console.log('Favicon handler');
  },
}


module.exports = novanoc_handlers;